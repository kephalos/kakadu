module kakadu.streams;

import std.stdio;
import core.exception;

interface IStream(T) {
  T next();
  bool atEnd();
}

interface IPositionableStream(T) : IStream!T {
  T peek();
  bool isEmpty();
  size_t myPosition();
}

class StringStream : IPositionableStream!char {
  string myBuffer;
  int myPos = -1;

  this(string s) {
    myBuffer = s;
  }

  char next() {
    return myBuffer[++myPos];
  }

  bool atEnd() {
    return myBuffer.length > myPos;
  }

  char peek() {
    return myBuffer[myPos + 1];
  }

  bool isEmpty() {
    return myBuffer.length < 1;
  }

  size_t myPosition() {
    return myPos;
  }

  unittest {
    auto s = new StringStream("abcd");

    assert(!s.atEnd());
    assert(s.next() == 'a');
    assert(s.next() == 'b');
    assert(s.next() == 'c');
    assert(s.next() == 'd');
    assert(s.atEnd());
  }

  unittest {
    auto s = new StringStream("abcd");

    assert(!s.isEmpty());

    assert(s.peek() == 'a');
    assert(s.next() == 'a');
    assert(s.next() == 'b');
    assert(s.next() == 'c');
    assert(s.next() == 'd');
  }

  unittest {
    auto s = new StringStream("");

    assert(s.isEmpty());
  }
}
