# Hacking kakadu

To build kakadu, execute

    cd kakadu
    autoreconf -vif && ./configure --prefix=/opt/kakadu && make
    . env

See the TODO file for pending tasks.

## Runing Tests


`make -e check` runs the tests
