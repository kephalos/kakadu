#ifndef LIBKAKADU_CORE_H
#define LIBKAKADU_CORE_H

struct klass {
	char *name;
};


struct object {
	struct klass *klass;
};

#endif
