#include "tokenizer.h"
#include <stdlib.h>

struct TbyList;

struct TokenContext {
	char *sourcePath;
	struct TbyList *list;
};

static struct TokenContext *
newsTokenContext(char *path)
{
	struct TokenContext *context = calloc(1, sizeof(*context));
	context->sourcePath = path;
	context->list = tbyListNew();
	return context;
}

static void
disposeTokenContext(struct TokenContext *context)
{
	free(context);
}

struct Token {
	struct TokenContext *context;
	char *literal;
	size_t line;
	size_t column;
};

typedef struct Token Token;

char *
tokenize(char *path, char *source)
{
	struct TokenContext *context = newsTokenContext(path);
	disposeTokenContext(context);
	return source;
}
